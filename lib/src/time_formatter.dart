import 'dart:math' show min;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class TimeInputFormatter extends TextInputFormatter {

  String _placeholder = "--:--";
  TextEditingValue? _lastNewValue;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    /// provides placeholder text when user start editing
    if (oldValue.text.isEmpty) {
      oldValue = oldValue.copyWith(
        text: _placeholder,
      );
      newValue = newValue.copyWith(
        text: _fillInputToPlaceholder(newValue.text),
      );
      return newValue;
    }

    if (newValue == _lastNewValue) {
      return oldValue;
    }
    _lastNewValue = newValue;

    int offset = newValue.selection.baseOffset;

    if (offset > 5) {
      return oldValue;
    }

    if (oldValue.text == newValue.text && oldValue.text.length > 0) {
      return newValue;
    }

    final String oldText = oldValue.text;
    final String newText = newValue.text;
    String? resultText;

    int index = _indexOfDifference(newText, oldText);
    if (oldText.length < newText.length) {
      String newChar = newText[index];
      if (index == 2) {
        index++;
        offset++;
      }
      resultText = oldText.replaceRange(index, index + 1, newChar);
      if (offset == 2) {
        offset++;
      }
    } else if (oldText.length > newText.length) {
      /// delete digit
      if (oldText[index] != ':') {
        resultText = oldText.replaceRange(index, index + 1, '-');
        if (offset == 3) {
          offset--;
        }
      } else {
        resultText = oldText;
      }
    }

    /// verify the number and position of splash character
    final splashes = resultText!.replaceAll(RegExp(r'[^:]'), '');
    int count = splashes.length;
    if (resultText.length > 5 ||
        count != 1 ||
        resultText[2].toString() != ':') {
      return oldValue;
    }

    return oldValue.copyWith(
      text: resultText,
      selection: TextSelection.collapsed(offset: offset),
      composing: defaultTargetPlatform == TargetPlatform.iOS
      // FIXME mettre a jour le SDK de flutter et ajouter const
        ? TextRange(start: 0, end: 0)
        : TextRange.empty,
    );
  }

  int _indexOfDifference(String? cs1, String? cs2) {
    if (cs1 == cs2) {
      return -1;
    }
    if (cs1 == null || cs2 == null) {
      return 0;
    }
    int i;
    for (i = 0; i < cs1.length && i < cs2.length; ++i) {
      if (cs1[i] != cs2[i]) {
        break;
      }
    }
    if (i < cs2.length || i < cs1.length) {
      return i;
    }
    return -1;
  }

  String _fillInputToPlaceholder(String? input) {
    if (input == null || input.isEmpty) {
      return _placeholder;
    }
    String result = _placeholder;
    final index = [0, 1, 3, 4];
    final length = min(index.length, input.length);
    for (int i = 0; i < length; i++) {
      result = result.replaceRange(index[i], index[i] + 1, input[i]);
    }
    return result;
  }

  static bool isValid(String value) {
    if (value.length != 5)
      return false;

    for (int i=0; i<5; ++i)
      if (value[i] == "-")
        return false;

    final List<String> parts = value.split(":");

    if (parts.length != 2)
      return false;

    int? h = int.tryParse(parts[0]);
    int? m = int.tryParse(parts[1]);

    if (h == null || m == null || (h < 0 || h > 23) || (m < 0 || m > 59))
      return false;
    return true;
  }

}