import 'dart:math';
import 'package:flutter/services.dart';

import 'number_input_formatter.dart';

/// pour formatter une quantité ou une somme
class ThousandsFormatter extends NumberInputFormatter {

  static final RegExp _digitOnlyRegex = RegExp(r'\d+');
  static final FilteringTextInputFormatter _digitOnlyFormatter =
  FilteringTextInputFormatter.allow(_digitOnlyRegex);

  final String separator;
  ThousandsFormatter([this.separator = " "]);

  @override
  String formatPattern(String digits) {
    StringBuffer sb = StringBuffer();
    int length = digits.length;
    int start = length % 3;

    if (start != 0)
      sb.write(digits.substring(0, start));

    for (; start < length; start += 3) {
      if (start != 0)
        sb.write(separator);
      sb.write(digits.substring(start, start + 3));
    }

    return sb.toString();
  }

  @override
  TextEditingValue formatValue(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return _digitOnlyFormatter.formatEditUpdate(oldValue, newValue);
  }

  @override
  bool isUserInput(String s) {
    return _digitOnlyRegex.firstMatch(s) != null;
  }

}


/* ************************************************************** */
/* ************************************************************** */
/* ******************** PHONE NUMBER **************************** */
/* ************************************************************** */
/* ************************************************************** */


class PhoneNumber8DigitFormatter extends NumberInputFormatter {

  static final RegExp _digitOnlyRegex = RegExp(r'\d+');
  static final FilteringTextInputFormatter _digitOnlyFormatter =
  FilteringTextInputFormatter.allow(_digitOnlyRegex);

  final String separator;
  PhoneNumber8DigitFormatter([this.separator = ' ']): super(11);

  @override
  String formatPattern(String digits) {
    StringBuffer buffer = StringBuffer();
    int offset = 0;
    int count = min(2, digits.length);
    final length = digits.length;
    for (; count <= length; count += min(2, max(1, length - count))) {
      buffer.write(digits.substring(offset, count));
      if (count < length) {
        buffer.write(separator);
      }
      offset = count;
    }
    return buffer.toString();
  }

  @override
  TextEditingValue formatValue(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return _digitOnlyFormatter.formatEditUpdate(oldValue, newValue);
  }

  @override
  bool isUserInput(String s) {
    return _digitOnlyRegex.firstMatch(s) != null;
  }

}

// s'adapte automatiquement au nombre de caractere
//class PhoneNumberAutoFormatter extends NumberInputFormatter {

//}
